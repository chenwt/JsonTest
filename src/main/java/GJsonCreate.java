import java.lang.reflect.Field;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GJsonCreate {
	public static void main(String[] args) {
		People people = new People();
		Object nullObject = null;
		people.setName("陈文韬");
		people.setAge(24);
		people.setSchool("gdut");
		people.setBirthDay("1992-11-15");
		people.setSubject(new String[]{"数学","英文"});
		
		people.setHas_girlfriend(false);
		people.setCar(nullObject);
		people.setHouse(nullObject);
		people.setComent("这是一个注释");
		
		people.setIgnore("不要看见我");

		
		//Gson gson = new Gson();
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setPrettyPrinting();
		gsonBuilder.setFieldNamingStrategy(new FieldNamingStrategy() {
			
			public String translateName(Field paramField) {
				if (paramField.getName().equals("name")) {
					return "NAME";
				}
				return paramField.getName();
			}
		});
		Gson gson = gsonBuilder.create();
		System.out.println(gson.toJson(people));
	}
}
