import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonObjectTest {
	
	
	public static void main(String[] args) {
		//JsonObject();
		//createJsonByHashMap();
		createJsonByBean(); 
	}

	private static void createJsonByBean() {
		People people = new People();
		Object nullObject = null;
		people.setName("陈文韬");
		people.setAge(24);
		people.setSchool("gdut");
		people.setBirthDay("1992-11-15");
		people.setSubject(new String[]{"数学","英文"});
		
		people.setHas_girlfriend(false);
		people.setCar(nullObject);
		people.setHouse(nullObject);
		people.setComent("这是一个注释");
		people.setIgnore("不要看见我");
		System.out.println(new JSONObject(people));
	}

	private static void createJsonByHashMap() {
		HashMap< String, Object> map = new HashMap< String, Object>();
		Object nullObject = null;
		map.put("name", "陈文韬");
		map.put("age", 24);
		map.put("school", "gdut");
		map.put("birthDay", "1992-11-15");
		map.put("subject", new String[]{"数学","英文"});
		
		map.put("has_girlfriend", false);
		map.put("car", nullObject);
		map.put("house", nullObject);
		map.put("Coment", "这是一个注释");
		
		System.out.println(new JSONObject(map).toString());
	}

	/*{
		"name":"陈文韬",
		"age":24,
		"school":"gdut",
		"subject":["数学","英文"],
		"has_girlfriend":false,
		"car":null,
		"house":null,
		"comment":"这是一个注释"
	}*/
	private static void JsonObject() {
		JSONObject people = new JSONObject();
		Object nullObject = null;
		try {
			people.put("name", "陈文韬");
			people.put("age", 24);
			people.put("school", "gdut");
			people.put("birthDay", "1992-11-15");
			people.put("subject", new String[]{"数学","英文"});
			
			people.put("has_girlfriend", false);
			people.put("car", nullObject);
			people.put("house", nullObject);
			people.put("Coment", "这是一个注释");
			
			System.out.println(people.toString());
		} catch (JSONException e) {
			
		}
	}
}


