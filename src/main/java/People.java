import com.google.gson.annotations.SerializedName;

public class People {
	//@SerializedName("NAME")
	private String name;
	private int age;
	private String school;
	private String birthDay;
	
	private transient String ignore;
	
	public String getIgnore() {
		return ignore;
	}

	public void setIgnore(String ignore) {
		this.ignore = ignore;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public String getSchool() {
		return school;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public String[] getSubject() {
		return subject;
	}

	public boolean isHas_girlfriend() {
		return has_girlfriend;
	}

	public Object getCar() {
		return car;
	}

	public Object getHouse() {
		return house;
	}

	public String getComent() {
		return Coment;
	}

	private String[] subject;
	
	private boolean has_girlfriend;
	private Object car;
	private Object house;
	
	private String Coment;

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public void setSubject(String[] subject) {
		this.subject = subject;
	}

	public void setHas_girlfriend(boolean has_girlfriend) {
		this.has_girlfriend = has_girlfriend;
	}

	public void setCar(Object car) {
		this.car = car;
	}

	public void setHouse(Object house) {
		this.house = house;
	}

	public void setComent(String coment) {
		Coment = coment;
	}
}
