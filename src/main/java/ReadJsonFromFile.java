import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class ReadJsonFromFile {
	public static void main(String[] args) throws IOException {
		File file = new File(ReadJsonFromFile.class.getResource("/json/people.json").getFile());
		String content = FileUtils.readFileToString(file, "utf8");
		JSONObject jsonObject = new JSONObject(content);
		System.out.println(jsonObject);
		
		jsonObject.isNull("key");
		
		JSONArray object = jsonObject.getJSONArray("subject");
		System.out.println(object);
	}
}
